"""

"""
import numpy as np
import fileinput


def read_node_rank():
    """

    :return:
    """

    matrix = []
    queries = []
    counts = []

    i = -1
    N = 0
    beta = 0
    max_iter = 0

    for line in fileinput.input():
        line = line.strip().split(' ')

        if i == -1:
            N = int(line[0])
            beta = float(line[1])

        elif i < N:
            nodes = np.array(line).astype(np.int)
            matrix.append(nodes)
            counts.append(len(nodes))

        elif i == N:
            i += 1
            continue

        else:
            query = int(line[0]), int(line[1])
            queries.append(query)

            max_iter = max(max_iter, query[1])

        i += 1

    return np.array(matrix), np.array(counts), queries, beta, max_iter


def read_black_node():
    """

    :return:
    """
    matrix = {}
    black_nodes = set()
    N = 0
    E = 0
    i = -1

    for line in fileinput.input():
        line = line.strip().split(" ")

        if i == -1:
            N, E = int(line[0]), int(line[1])
            i += 1

        elif i < N:
            if int(line[0]) == 1:
                black_nodes.add(i)

            i += 1

        else:
            u, v = int(line[0]), int(line[1])
            matrix[u] = [v] if u not in matrix else matrix[u] + [v]
            matrix[v] = [u] if v not in matrix else matrix[v] + [u]
            i += 1

    return matrix, black_nodes


