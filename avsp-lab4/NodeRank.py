"""
"""
import utils
import numpy as np


def execute_query(index, iter, rank_matrix):
    """

    :param index:
    :param iter:
    :param rank_matrix:
    :return:
    """
    return rank_matrix[iter][index]


def calculate_node_rank(node_matrix, counts, beta, max_iter):
    """

    :param node_matrix:
    :param counts:
    :param beta:
    :param max_iter:
    :return:
    """
    N = len(node_matrix)
    rank_matrix = np.zeros((max_iter + 1, N))

    rank_matrix[0] += 1 / N

    for i in range(0, max_iter):
        rank_matrix[i + 1] += (1 - beta) / N

        for node in range(len(node_matrix)):
            value = beta * rank_matrix[i][node] / len(node_matrix[node])

            rank_matrix[i + 1][node_matrix[node]] += value

    return rank_matrix


if __name__ == '__main__':
    matrix, counts, queries, beta, max_iter = utils.read_node_rank()

    rank_matrix = calculate_node_rank(matrix, counts, beta, max_iter)

    for query in queries:
        print("%.10f" % execute_query(query[0], query[1], rank_matrix))
