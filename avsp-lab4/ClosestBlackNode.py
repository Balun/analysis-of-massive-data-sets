"""

"""

import utils


def nearest_black_node(visited_set, opened_set, distance):
    """

    :param visited_set:
    :param opened_set:
    :param distance:
    :return:
    """
    if distance > 10 or len(opened_set) == 0:
        return -1, -1

    next_set = set()
    opened_set = sorted(opened_set)

    visited_set = visited_set.union(opened_set)

    for node in opened_set:
        if node in black_nodes:
            return node, distance

        next_set = set(sorted(next_set.union(set([n for n in matrix[node] if n
                                                  not in visited_set and n not \
                                                  in dead_ends]))))

    return nearest_black_node(set(sorted(visited_set)), set(sorted(next_set)),
                              distance + 1)


if __name__ == '__main__':
    matrix, black_nodes = utils.read_black_node()
    dead_ends = set()

    for node in range(len(matrix)):

        b, dist = nearest_black_node(set(), {node}, 0)

        if b == -1:
            dead_ends.add(node)

        print(b, dist)
