"""
"""
import utils
import numpy as np


def recommend_query(I, J, T, K, matrix, matrix_t, matrix_n, matrix_n_t):
    """

    :param I:
    :param J:
    :param T:
    :param K:
    :param matrix:
    :return:
    """
    original_matrix = matrix if T == 0 else matrix_t

    matrix, avg = matrix_n if T == 0 else matrix_n_t
    square_sums = np.sqrt(np.sum(np.square(matrix), axis=1))

    I -= 1
    J -= 1

    item_index = I if T == 0 else J
    user_index = J if T == 0 else I

    results = dict()
    results[item_index] = (1, matrix[item_index][user_index])

    owner_rating = square_sums[item_index]

    for i in range(len(matrix)):
        if i == item_index:
            continue

        other_rating = square_sums[i]
        rating_sum = np.dot(matrix[item_index], matrix[i])

        result = rating_sum / (owner_rating * other_rating)
        grade = original_matrix[i][user_index]

        results[i] = (result, grade)

    similarities = [value for key, value in results.items() if key !=
                    item_index and value[1] != 0 and value[0] > 0]

    similarities = sorted(similarities, reverse=True, key=lambda x: x[0])[0:K]
    similarity_result = sum([val[0] for val in similarities])
    sim_result_grade = sum([val[0] * val[1] for val in
                            similarities])

    return sim_result_grade / similarity_result


if __name__ == '__main__':
    matrix, querries = utils.read_input()

    matrix_t = np.transpose(matrix)
    matrix_n = utils.normalize_matrix(matrix)
    matrix_n_t = utils.normalize_matrix(matrix, True)

    for q in querries:
        utils.print_half_up(recommend_query(q[0], q[1], q[2], q[3], matrix,
                                            matrix_t,
                                            matrix_n,
                                            matrix_n_t))
