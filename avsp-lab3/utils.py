"""
"""

import fileinput
import numpy as np

from decimal import Decimal, ROUND_HALF_UP


def read_input():
    """

    :return:
    """
    matrix = []
    querries = []

    N, M, Q = None, None, None
    i = 0

    for line in fileinput.input():
        line = line.strip()

        if i == 0:
            N = int(line.split(" ")[0])
            M = int(line.split(" ")[1])

        elif i < N + 1:
            data = np.array(line.split(" "))

            data[data == 'X'] = 0
            data = data.astype(np.float)

            matrix.append(data)

        elif i == N + 1:
            Q = int(line)

        else:
            querries.append(np.array(line.split(" "), dtype=np.int))

        i += 1

    return np.array(matrix), np.array(querries)


def normalize_matrix(matrix, transpose=False):
    """

    :param matrix:
    :return:
    """
    matrix = np.copy(matrix)
    matrix = np.transpose(matrix) if transpose else matrix
    avg = []
    for row in matrix:
        avg.append(np.average(row[row != 0]))

    for i, row in enumerate(matrix):
        row[row != 0] -= avg[i]

    return matrix, np.array(avg)


def print_half_up(value):
    """

    :param value:
    :return:
    """
    print(Decimal(Decimal(value).quantize(Decimal('.001'),
                                          rounding=ROUND_HALF_UP)))
