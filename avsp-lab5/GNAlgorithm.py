"""
"""

import utils


def calculate_betweenness(edges, features, n_features):
    """

    :param edges:
    :param vertices:
    :return:
    """
    edges = utils.calculate_weights(edges, features, n_features)

    distances, path_matrix = utils.floyd_warshall_mutiple(edges,
                                                          features.keys())

    paths = utils.get_shortest_paths_multiple(features.keys(), path_matrix)

    centrality = utils.get_centrality_multiple(paths.values(), edges.keys())

    return centrality, path_matrix


def get_edges_with_highest_betweenness(betweenness):
    """

    :param betweenness:
    :return:
    """
    max_b = max(betweenness.items(), key=lambda e: e[1])

    return [(u, v) for (u, v), b in betweenness.items() if b == max_b[1]]


def modularity(vertices, edges, path_matrix):
    """

    :param vertices:
    :param edges:
    :return:
    """
    weight_sum = sum(edges.values())
    Q = 0

    for u in vertices:
        for v in vertices:
            if u == v:
                A_uv = 0
            elif (u, v) not in edges:
                if (v, u) in edges:
                    A_uv = edges[v, u]
                else:
                    A_uv = 0
            else:
                A_uv = edges[u, v]

            k_u = sum(w for (i, j), w in edges.items() if u == j or u == i)
            k_v = sum(w for (i, j), w in edges.items() if v == j or v == i)

            delta = len(utils.reconstruct_path_multiple(u, v, path_matrix)) \
                    != 0

            Q += (A_uv - (k_v * k_u) / (2 * weight_sum)) * int(delta)

    return round(Q / (2 * weight_sum), 4)


if __name__ == '__main__':
    adj_matrix, features, edges, n_features = utils.read_data()
    edges = utils.calculate_weights(edges, features, n_features)
    comms = {}

    while edges:
        adj_matrix = utils.get_adj_matrix(edges.keys())
        edge_betweenness, path_matrix = calculate_betweenness(edges,
                                                              features,
                                                              n_features)

        mod = modularity(features.keys(), edges, path_matrix)
        comms[mod] = utils.find_communities(features.keys(), adj_matrix)

        edges_to_remove = get_edges_with_highest_betweenness(edge_betweenness)

        if len(edges_to_remove) == 1:
            e = edges_to_remove[0]
            sorted_e = sorted(e, key=lambda x: int(x))

            print(sorted_e[0], sorted_e[1])
            del edges[e]

        else:
            for u, v in sorted([sorted(e, key=lambda x: int(x)) for e in
                                edges_to_remove],
                               key=lambda e: (int(e[0]), int(e[1]))):
                print(u, v)
                try:
                    del edges[(u, v)]
                except KeyError:
                    del edges[(v, u)]

    max_mod, comms = max(comms.items(), key=lambda i: i[0])

    comms = [sorted(c, key=lambda x: int(x)) for c in comms]
    comms = sorted(comms, key=lambda c: (len(c), int(c[0])))

    line = ""
    for comm in comms:
        if len(comm) == 1:
            line += comm[0] + " "

        else:
            for v in sorted(comm, key=lambda x: int(x)):
                line += v + "-"

            line = line[:-1]
            line += " "

    print(line.strip())
