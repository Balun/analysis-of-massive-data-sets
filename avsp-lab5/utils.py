"""
"""

import fileinput
import numpy as np


def read_data():
    """

    :return:
    """
    adj_matrix = {}
    feat_matrix = {}
    edges = []
    n_features = 0

    feature_field = False

    for line in fileinput.input():
        line = line.strip()

        if not line:
            feature_field = True
            continue

        line = line.split(" ")

        if not feature_field:
            adj_matrix[line[0]] = [line[1]] if line[0] not in adj_matrix \
                else adj_matrix[line[0]] + [line[1]]

            adj_matrix[line[1]] = [line[0]] if line[1] not in adj_matrix \
                else adj_matrix[line[1]] + [line[0]]

            edges.append((line[0], line[1]))

        else:
            n_features = len(line) - 1
            feat_matrix[line[0]] = np.array(line[1:], dtype=np.int8)

    return adj_matrix, feat_matrix, edges, n_features


def calculate_weights(edges, features, max_sim):
    """

    :param edges:
    :param features:
    :return:
    """
    weight_edges = {}

    for edge in edges:
        u, v = features[edge[0]], features[edge[1]]
        sim = np.sum(u == v)

        weight_edges[(edge[0], edge[1])] = sim

    return {(u, v): max_sim - (sim - 1) for (u, v), sim in
            weight_edges.items()}


def floyd_warshall_mutiple(edges, vertices):
    distances = {(u, v): float("inf") for u in vertices for v in vertices}
    next_matrix = {}

    for (u, v), w in edges.items():
        distances[(u, v)] = distances[(v, u)] = w
        next_matrix[(u, v)] = [v]
        next_matrix[(v, u)] = [u]

    for v in vertices:
        distances[(v, v)] = 0
        next_matrix[(v, v)] = [v]

    for k in vertices:
        for i in vertices:
            for j in vertices:
                if distances[(i, j)] > distances[(i, k)] + distances[(k, j)]:
                    distances[(i, j)] = distances[(i, k)] + distances[(k, j)]
                    next_matrix[(i, j)] = [k]

                elif distances[i, k] + distances[k, j] == distances[i, j] and \
                        distances[i, j] != float("inf") and k != j and k != i:

                    if (i, j) in next_matrix:
                        next_matrix[(i, j)].append(k)

                    else:
                        next_matrix[(i, j)] = [k]

    return distances, next_matrix


def get_shortest_paths_multiple(vertices, path_matrix):
    """

    :param vertices:
    :param path_matrix:
    :return:
    """
    return {(u, v): reconstruct_path_multiple(u, v, path_matrix) for u in
            vertices for
            v in
            vertices}


def reconstruct_path_multiple(u, v, path_matrix):
    """

    :param u:
    :param v:
    :param path_matrix:
    :return:
    """
    all_paths = []

    if (u, v) not in path_matrix:
        return all_paths

    for k in path_matrix[(u, v)]:
        if k == v:
            all_paths.append([u, v])

        else:
            paths_u_k = reconstruct_path_multiple(u, k, path_matrix)
            paths_k_v = reconstruct_path_multiple(k, v, path_matrix)

            for u_k in paths_u_k:
                for k_v in paths_k_v:
                    u_k = u_k[:-1]
                    all_paths.append(u_k + k_v)

    return all_paths


def get_centrality_multiple(paths, edges):
    centrality = {(u, v): 0 for u, v in edges}

    for mul_path in paths:
        if not mul_path:
            continue

        N = len(mul_path)

        for path in mul_path:
            prev = path[0]
            for v in path[1:]:
                if (prev, v) in centrality:
                    centrality[(prev, v)] = round(centrality[prev,
                                                             v] + round(
                        1 / N, 4), 4)

                elif (v, prev) in centrality:
                    centrality[(v, prev)] = round(centrality[v, prev] +
                                                  round(1 / N, 4), 4)

                prev = v

    return {(u, v): round(c / 2, 4) for (u, v), c in centrality.items()}


def dfs_util(v, visited, adj_matrix):
    comm = [v]
    visited[v] = True

    try:
        for i in adj_matrix[v]:
            if visited[i] == False:
                comm += dfs_util(i, visited, adj_matrix)
    except KeyError:
        pass

    return comm


def fill_order(v, visited, stack, adj_matrix):
    visited[v] = True

    try:
        for u in adj_matrix[v]:
            if visited[u] == False:
                fill_order(u, visited, stack, adj_matrix)
    except KeyError:
        pass

    stack.append(v)


def find_communities(vertices, adj_matrix):
    comms = []
    stack = []
    visited = {v: False for v in vertices}

    for u in vertices:
        if visited[u] == False:
            fill_order(u, visited, stack, adj_matrix)

    visited = {v: False for v in vertices}

    while stack:
        i = stack.pop()
        if visited[i] == False:
            comm = dfs_util(i, visited, adj_matrix)
            comms.append(comm)

    return comms


def get_adj_matrix(edges):
    adj_matrix = {}

    for u, v in edges:
        adj_matrix[u] = [v] if u not in adj_matrix \
            else adj_matrix[u] + [v]

        adj_matrix[v] = [u] if v not in adj_matrix \
            else adj_matrix[v] + [u]

    return adj_matrix
