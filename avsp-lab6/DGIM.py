"""
"""
import fileinput

import numpy as np


def resolve_query(k, timer, buckets):
    """

    :param k:
    :param timer:
    :param buckets:
    :return:
    """
    candidates = [b for b in buckets if timer - b[0] < k]

    if len(candidates) <= 1:
        return 0

    ones_sum = sum([b[1] for b in candidates[1:]])

    last = candidates[0]
    return ones_sum + int(last[1] / 2)


def remove_old_buckets(timer, buckets, N):
    """

    :param timer:
    :param buckets:
    :return:
    """
    to_remove = [b for b in buckets if timer - b[0] == N]
    return [b for b in buckets if b not in to_remove]


def merge_buckets(buckets):
    """

    :param buckets:
    :return:
    """
    max_size = max(buckets, key=lambda b: b[1])[1]

    size = 1
    while size <= max_size:
        current_size = size

        layer = [b for b in buckets if b[1] == current_size]

        if len(layer) <= 2:
            break

        new_bucket = layer[2]

        a = layer[0]
        b = layer[1]

        new_time_stamp = b[0]
        new_size = a[1] * 2

        merged_bucket = (new_time_stamp, new_size)

        buckets.insert(buckets.index(new_bucket), merged_bucket)
        buckets.remove(a)
        buckets.remove(b)

        size *= 2

    return buckets


if __name__ == '__main__':
    N = 0
    buckets = []
    timer = 0

    i = 0
    for line in fileinput.input():
        line = line.strip().split(" ")

        if len(line) == 1:
            if i == 0:
                N = int(line[0])
                i += 1
                continue

            bits = np.array(list(line[0]), dtype=np.int8)

            for bit in bits:
                timer += 1

                buckets = remove_old_buckets(timer, buckets, N)

                if bit:
                    buckets.append((timer, 1))  # add new bucket with default
                    #  size 1
                    buckets = merge_buckets(buckets)

        else:
            k = int(line[1])
            print(resolve_query(k, timer, buckets))

        i += 1
