"""

"""


def hamming_dist(first, second):
    """

    :param first:
    :param second:
    :param k:
    :return:
    """
    assert len(first) == len(second)

    count, z = 0, int(second, 16) ^ int(first, 16)

    while z:
        count += 1
        z &= z - 1

    return count


def hex_to_bin(value, size=128):
    """

    :param value
    :param size:
    :return:
    """
    return bin(int(value, 16))[2:].zfill(size)


def bin_to_int(bit_array):
    """

    :param bit_array:
    :return:
    """
    i = 0
    for bit in [int(b) for b in bit_array]:
        i = (i << 1) | bit

    return i


def int_to_hex(value, size=32):
    """

    :param value:
    :param size:
    :return:
    """
    hex_value = hex(value)[2:]

    if len(hex_value) != size:
        for _ in range(size - len(hex_value)):
            hex_value = "0" + hex_value

    return hex_value


def bin_to_hex(value, size=32):
    """

    :param value:
    :param size:
    :return:
    """
    return int_to_hex(bin_to_int(value), size=size)


def hex_to_int(value):
    """

    :param value:
    :return:
    """
    return int(value, 16)
