"""
"""
import hashlib
import fileinput

import src.utils as utils


def simhash(text):
    """

    :param text:
    :return:
    """
    sim_hash = 128 * [0]

    for element in text.split(" "):
        md5 = hashlib.md5(element.encode('utf-8')).hexdigest()

        md5 = utils.hex_to_bin(md5)

        sim_hash = [sh + 1 if int(h) == 1 else sh - 1 for sh, h in
                    zip(sim_hash, md5)]

    sim_hash = [1 if sh >= 0 else 0 for sh in sim_hash]

    return utils.bin_to_hex(sim_hash, 32)


if __name__ == '__main__':
    N = 0
    Q = 0

    hashes = []
    sims = []

    i = 1
    for line in fileinput.input():
        line = line.strip()

        if i == 1:
            N = int(line)

        elif i == N + 2:
            Q = int(line)

        elif i < N + 2:
            hashes.append(simhash(line))

        else:
            index = int(line.split()[0])
            k = int(line.split()[1])

            count = 0

            for j in range(len(hashes)):
                if j != index:
                    if utils.hamming_dist(hashes[index], hashes[j]) <= k:
                        count += 1

            sims.append(count)

        i += 1

    for num in sims:
        print(num)
