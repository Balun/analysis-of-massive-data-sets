"""

"""

import fileinput
import src.utils as utils
import src.SimHash as sh

SLICES = 8


def hash_2_int(slice, value):
    """

    :return:
    """
    width = 128 / SLICES
    value = utils.hex_to_bin(value)

    end = int(width * (slice + 1))
    start = int(width * slice)

    value = value[start:end]

    return utils.bin_to_int(value)


def lsh(hashes):
    """

    :param hashes:
    :return:
    """
    candidates = {i: set() for i in range(len(hashes))}

    for slice in range(SLICES):
        buckets = {}

        for current_id in range(len(hashes)):
            hash = hashes[current_id]
            value = hash_2_int(slice, hash)

            if value in buckets:
                texts = buckets[value]

                for id in texts:
                    candidates[current_id].add(id)
                    candidates[id].add(current_id)

            else:
                texts = set()

            texts.add(current_id)
            buckets[value] = texts

    return candidates


if __name__ == '__main__':
    N = 0
    Q = 0

    hashes = {}
    sims = []
    candidates = {}

    i = 1
    hash_ind = 0

    for line in fileinput.input():
        line = line.strip()

        if i == 1:
            N = int(line)

        elif i == N + 2:
            Q = int(line)
            candidates = lsh(hashes)

        elif i < N + 2:
            hashes[hash_ind] = sh.simhash(line)
            hash_ind += 1

        else:
            index = int(line.split()[0])
            k = int(line.split()[1])

            count = 0

            for j in candidates[index]:
                if j != index:
                    dist = utils.hamming_dist(hashes[index], hashes[j])

                    if dist <= k:
                        count += 1

            sims.append(count)

        i += 1

    for num in sims:
        print(num)
