import unittest as ut
import sys

import src.SimHash as sh
import src.utils as utils
import src.SimHashBuckets as buckets


class SimHashTest(ut.TestCase):
    """

    """

    def test_first(self):
        """

        :return:
        """
        N = 0
        Q = 0

        hashes = []
        sims = []

        i = 1
        for line in open('../res/data/1A/test2/R.in', 'r'):
            line = line.strip()

            if i == 1:
                N = int(line)

            elif i == N + 2:
                Q = int(line)

            elif i < N + 2:
                hashes.append(sh.simhash(line))

            else:
                index = int(line.split()[0])
                k = int(line.split()[1])

                count = 0

                for j in range(len(hashes)):
                    if j != index:
                        if utils.hamming_dist(hashes[index], hashes[j]) <= k:
                            count += 1

                sims.append(count)

            i += 1

        for num, rez in zip(sims, open('../res/data/1A/test2/R.out', 'r')):
            if num != int(rez.strip()):
                raise ValueError("ERROR: %d != %d" % (num, int(rez.strip())))

    def test_second(self):
        """

        :return:
        """
        N = 0
        Q = 0

        hashes = {}
        sims = []
        candidates = {}

        i = 1
        hash_ind = 0

        for line in open('../res/data/1B/test2/R.in', 'r'):
            line = line.strip()

            if i == 1:
                N = int(line)

            elif i == N + 2:
                Q = int(line)
                candidates = buckets.lsh(hashes)

            elif i < N + 2:
                hashes[hash_ind] = sh.simhash(line)
                hash_ind += 1

            else:
                index = int(line.split()[0])
                k = int(line.split()[1])

                count = 0

                for j in candidates[index]:
                    if j != index:
                        dist = utils.hamming_dist(hashes[index], hashes[j])

                        if dist <= k:
                            count += 1

                sims.append(count)

            i += 1

        print("TU SAM")
        for num, rez in zip(sims, open('../res/data/1B/test2/R.out', 'r')):
            if num != int(rez.strip()):
                raise ValueError("ERROR: %d != %d" % (num, int(rez.strip())))
